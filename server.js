//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
let bodyparser = require('body-parser');
app.use(bodyparser.json())
app.use((req, res, next)=>{
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next()
})

let movimientosV2JSON = require('./movimientosv2.json');

let requestjson = require('request-json');
let urlClientesMLab = "https://api.mlab.com/api/1/databases/mcortez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
let clienteMLab = requestjson.createClient(urlClientesMLab)

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', (req, res)=>{
  //res.status(200).send({"message": 'Hemos recibido su petición'})
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.post('/', (req, res)=>{
  res.status(201).send({"message": "Hemos recibido su petición POST"})
})

app.put('/', (req, res)=>{
  res.status(201).send({"message": "Hemos recibido su petición PUT"})
})

app.delete('/', (req, res)=>{
  res.status(201).send({"message": "Hemos recibido su petición DELETE"})
})

app.get('/v1/Clientes/:idCliente', (req, res)=>{
  res.send('Aquí tiene al cliente numero: ' + req.params.idCliente)
})

app.get('/v1/movimientos', (req, res)=>{
  res.status(200).sendFile(path.join(__dirname, 'movimientosv2.json'))
})

app.get('/v2/movimientos', (req, res)=>{
  res.status(200).json(movimientosV2JSON)
})

app.get('/v2/movimientos/:index', (req, res)=>{
  console.log(req.params.index);
  res.status(200).send(movimientosV2JSON[req.params.index-1])
})

app.get('/v2/movimientosquery', (req, res)=>{
  console.log(req.query);
  res.send('recibido')
})

app.post('/v2/movimientos', (req, res)=>{
  let nuevo = req.body
  nuevo.id = movimientosV2JSON.length +1
  movimientosV2JSON.push(nuevo)
  console.log(nuevo);
  res.status(201).send('movimiento dado de alta')
})

app.put('/v2/movimientos', (req, res)=>{
  res.status(200).send('Hemos recibido tu petición de actualización de movimientos')
})

app.get('/v3/Clientes', (req, res)=>{
  clienteMLab.get('', (err, resM, body)=>{
    if (err) {
      console.log(body);
    }else{
      res.send(body)
    }
  })
})

app.post('/v3/Clientes', (req, res)=>{
  clienteMLab.post('', req.body, (err, resM, body)=>{
    if (err) {
      console.log(body);
    }else{
      res.send(body)
    }
  })
})
